# README #
Flw is a simple javascript library that manages to handle 2-way databinding.
The library does not require any additional libraries to run, only vanilla javascript.

## Size
The library is only 4.6 kB minified.

## Version ##
Current version is **0.1.0**

## Installation ##
Include flw.min.js into your project.
Once included, instantiate `Flw` object and fill it with `Elem` objects. Lastly, run the application with `.run()` method.


```
let app = new Flw('.targetClass');
let main = new Elem('mainTemplate');

app.addElem(main, 'mainKey');

app.run();
```

## Contact ##
- Bitbucket: https://bitbucket.org/Jocho
- Github:    https://github.com/Mystael
- E-mail:    [marek.kolcun@gmail.com](marek.kolcun@gmail.com)