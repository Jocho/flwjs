/**
 * Flw - wrap bindings faster
 *
 * Flw is a simple wrapper to handle 2-way databinding.
 *
 * @author Marek J. Kolcun
 * @version 0.4.1
 */

/**
 * Main Flw class.
 *
 * This class generates an instance of main object that handles
 * all the other flw-based elements.
 *
 * @public
 * @class
 */
class Flw {
    /**
     * A Flw constructor method.
     *
     * @param {string} rootSelector - an argument for `document.querySelector()`
     * @param {object} elems        - [opt] object-list of pre-generated Elems
     */
    constructor(rootSelector, elems) {
        this.root = rootSelector.indexOf('.') === 0
            ? document.getElementsByClassName(rootSelector.substring(1))[0]
            : document.getElementById(rootSelector.substring(1));
        this.elems = new Map();
        elems = elems || {};

        for (let i in elems) {
            if (!elems.hasOwnProperty(i)) {
                continue;
            }
            elems[i].renderer.root = this.root;
            this.addElem(elems[i], i);
        }
    }

    info() {
        return {
            author: 'Marek J. Kolcun',
            version: '0.4.1',
            changed: '2025-02-22T16:31:00Z'
        };
    }

    /**
     * Adds an Elem instance info list of handled objects.
     *
     * @public
     * @param {Elem} elem - an Elem class instance
     * @param {string} id - identifier
     */
    addElem(elem, id = null) {
        id = id || elem.getId();
        elem.setId(id);
        elem.renderer.root = this.root;
        this.elems.set(id, elem);
    }

    getElem(id) {
        if (this.hasElem(id)) {
            return this.elems.get(id);
        }
        return null;
    }

    /**
     * Removes an Elem instance from list of handled objects.
     *
     * @public
     * @param {string} id - Elem identifier
     */
    removeElem(id) {
        if (!this.hasElem(id))
            return;

        this.elems.delete(id);
        this.root.querySelector('[data-id="' + id + '"').remove();
    }

    /**
     * Checks whether an Elem instance with given `id` is already handled.
     *
     * @public
     * @param {string} id - Elem identifier
     * @return {bool}     - true if `id` is already registered
     */
    hasElem(id) {
        return this.elems.has(id);
    }

    /**
     * Main method that launches all the Flw functionality.
     *
     * @public
     */
    run() {
        while (this.root.childNodes.length) {
            this.root.firstChild.remove();
        }

        this.elems.forEach((x, y) => {
            x.renderer.dom = x.renderer.render();
            this.root.appendChild(x.renderer.dom);
        });
    }
}

/**
 * Elem is a basic building block of Flw functionality.
 *
 * Every Elem object handles part of the application functionality, being it a
 * form, a login block, a shopping cart, a list of goods, etc.
 *
 * @public
 * @class
 */
class Elem {
    /**
     * An Elem constructor method.
     *
     * @param {string} templateName - it may be either a name of the template,
     *                                or a Text node content
     * @param {object} props        - object carrying all the props given to Elem
     * @param {object} hooks        - object carrying all the methods that can be
     *                                triggered via EventListeners
     * @param {bool} useDOM         - trigger that sets whether Renderer uses
     *                                existing DOM or generates a new one from
     *                                scratch
     */
    constructor(templateName, props = {}, hooks = {}, useDOM = true) {
        this.templateName = templateName;
        this.props = props;
        this.hooks = hooks;
        this.id = this.generateId();
        this.renderer = new Renderer(this, useDOM);
    }

    /**
     * Generates a random id with given prefix.
     *
     * @private
     * @param {string} prefix - basically any string
     */
    generateId() {
        return [
            (new Date()).getTime(),
            new String(Math.floor(Math.random() * 10000)).padStart(4, '0')
        ].join('_');
    }

    /**
     * Sets the Elem's `id` identifier.
     *
     * @private
     * @param {string} id - new Elem id
     */
    setId(id) {
        this.id = id;
    }

    /**
     * Returns Elem's `id` identifier.
     *
     * @public
     * @return {string} - Elem's id
     */
    getId() {
        return this.id;
    }

    /**
     * Checks whether a value with given path exists in Elem props object.
     *
     * @param {*} path      - a dot-separated path to the props attribute
     * @param {object} data - used for recursive call
     * @return {bool}       - true if the path returns variable, false otherwise
     */
    has(path, data = null) {
        path = path instanceof Array ? path : path.split('.');
        data = data || this.props;

        let part = path.shift();

        if (typeof data[part] === 'undefined') {
            return false;
        }

        if (path.length) {
            return data[part] instanceof Elem
                ? data[part].has(path)
                : this.has(path, data[part]);
        }
        else {
            return true;
        }
    }

    /**
     * Returns props value by given path.
     *
     * @public
     * @param {string} path - a dot-separated path to the props attribute
     * @param {object} data - used for recursive call
     * @return {mixed}      - returns value of the targeted props attribute
     */
    get(path, data = null) {
        path = path instanceof Array ? path : path.split('.');
        data = data || this.props;

        let part = path.shift();

        if (typeof data[part] === 'undefined') {
            return null;
        }

        if (path.length) {
            return data[part] instanceof Elem
                ? data[part].get(path)
                : this.get(path, data[part]);
        }
        else {
            return data[part];
        }
    }

    /**
     * Sets the value into Elem's props by given path.
     *
     * @public
     * @param {string} path - dot-separated path to the props attribute; if the path
     *                        doesn't exist, it will be created.
     * @param {mixed} value - value to be stored in the props
     * @param {object} data - used for recursive call
     * @param {string} orig - original path value used as first argument of `update()` method
     */
    set(path, value, data = null, orig = null) {
        orig = orig || path;
        path = path instanceof Array ? path : path.split('.');
        data = data || this.props;

        let part = path.shift();

        if (path.length) {
            data[part] instanceof Elem
                ? data[part].set(path, value, null, path.join('.'))
                : this.set(path, value, data[part] || {}, orig);
        }
        else {
            data[part] = value;
            this.renderer.update(orig);
        }
    }

    /**
     * Alters the values of the Elem's props on the given path, while not interacting
     * with other props's object attributes.
     *
     * The difference between `set()` and `mod()` is that `set()` rewrites all the data
     * structures on given path whilst `mod()` merges the objects together.
     *
     * E.g.
     * ```
     * let person = new Elem('', {name: 'John', job: {position: 'programmer', years: 3}});
     * console.log(person.get('job')); // {position: 'programmer', years: 3}
     * person.mod('job', {years: 5});
     * console.log(person.get('job')); // {position: 'programmer', years, 5}
     * person.set('job', {years: 7});
     * console.log(person.get('job')); // {years, 5}
     * ```
     *
     * @public
     * @param {string} path - dot-separated path to the props attribute; if the path
     *                        doesn't exist, it will be created.
     * @param {mixed} value - value to be stored in the props
     * @param {object} data - used for recursive call
     * @param {string} orig - original path value used as first argument of `update()` method
     */
    mod(path, value, data = null, orig = null) {
        orig = orig || path;
        path = path instanceof Array ? path : path.split('.');
        data = data || this.props;

        let part = path.shift();

        if (path.length) {
            data[part] instanceof Elem
                ? data[part].mod(path, value, null, path.join('.'))
                : this.mod(path, value, data[part] || {}, orig);
        }
        else {
            data[part] = {...data[part], ...value};
            this.renderer.update(orig);
        }
    }

    /**
     * Removes the value from the element located at given path.
     *
     * @public
     * @param {string} path - dot-separated path to the props attribute; if the path
     *                        does not exist, element will be re-rendered only.
     * @param {object} data - used for recursive call
     * @param {string} orig - original path value used as first argument of `update()` method
     */
    del(path, data = null, orig = null) {
        orig = orig || path;
        path = path instanceof Array ? path : path.split('.');
        data = data || this.props;

        let part = path.shift();

        if (path.length) {
            data[part] instanceof Elem
                ? data[part].del(path)
                : this.del(path, data[part], orig);
        }
        else {
            if (data instanceof Array) {
                data.splice(parseInt(part), 1);
            }
            else {
                delete(data[part]);
            }
            this.renderer.update(orig);
        }
    }
}

/**
 * A class that renders Elems into the DOM.
 *
 * Main class that handles Elem internal template and transforms it into the
 * HTML element that is then passed into DOM.
 *
 * @public
 * @class
 */
class Renderer {
    /**
     * An Elem constructor method.
     */
    constructor(elem, useDOM) {
        this.root = null;
        this.elem = elem;
        this.template = this.findTemplate(elem.templateName);
        this.dom = null;
        this.useDOM = typeof useDOM !== 'undefined' ? useDOM : true;
        this.debug = false;
    }

    /**
     *
     * Writes data to the console if debug mode of the Renderer is turned on.
     *
     * @param {*} text           - text to display if the debug mode is on.
     * @param {string|null} type - allows to format the console output. Currently
     *                             allowed values:
     *                              - > ........ console.group
     *                              - < ........ console.groupEnd
     *                              - ! ........ console.warn
     *                              - E ........ console.error
     *                              - null|* ... console.log
     */
    dbgr(text, type) {
        type = type || null;

        if (this.debug) {
            switch (type) {
                case 'E': console.error(text); break;
                case '!': console.warn(text); break;
                case '>': console.group(text); break;
                case '<': console.groupEnd(); break;
                default: console.log(text); break;
            }
        }
    }

    /**
     * Tries to find a <template> defined in HTML by its name; if the template
     * is not found, it will try to generate a node from given text, either
     * Element or a Text node.
     *
     * @private
     * @param {string} templateName - name of the <template> to be found
     * @return {Element|Text}       - a node to be returned
     */
    findTemplate(templateName) {
        let template = document.getElementsByTagName('template')[templateName] || null;
        if (template !== null) {
            return template.content.cloneNode(true)
        }
        else {
            const tmp = document.createElement('div');
            tmp.innerHTML = templateName;

            return tmp.childNodes[0];
        }
    }

    /**
     * Generates a DOM interpretation of an Elem instance.
     *
     * @public
     * @param {Node} node - used for recursive call
     */
    render(tmpl) {
        const updateDOM = typeof tmpl === 'undefined';
        tmpl = tmpl || this.template.cloneNode(true);

        if (tmpl instanceof DocumentFragment) {
            [...tmpl.childNodes].forEach((x) => {
                x.replaceWith(this.render(x));
            });
        }
        else if (tmpl instanceof Element) {
            // conditional rendering
            if (typeof tmpl.dataset.if !== 'undefined') {
                let condition = this.elem.get(tmpl.dataset.if);

                if (
                    null === condition
                    || (typeof condition === 'boolean' && !condition)
                    || (typeof condition === 'number' && !condition)
                    || (condition instanceof Number && !condition)
                    || (condition instanceof String && !condition.length)
                    || (condition instanceof Array && !condition.length)
                ) {
                    tmpl = new Text();
                    if (updateDOM) {
                        this.dom = tmpl;
                    }
                    return tmpl;
                }

                delete (tmpl.dataset.if);
            }

            // id
            if (typeof tmpl.dataset.id !== 'undefined') {
                tmpl.dataset.id = this.elem.getId();
            }

            // events
            if (typeof tmpl.dataset.e !== 'undefined') {
                this.findAndInvokeEvents(tmpl, tmpl.dataset.e);
                delete (tmpl.dataset.e);
            }
            // autobind form element events
            else {
                this.invokeAutoEvents(tmpl);
            }

            let renderChildren = true;

            // content defined via `data-content`
            if (typeof tmpl.dataset.content !== 'undefined') {
                [...tmpl.childNodes].forEach((chld) => {
                    tmpl.removeChild(chld);
                });

                let childContent = this.findAndReplaceBindings(tmpl.dataset.content);


                // replace child nodes with rendered nodes from childContent
                [...childContent.childNodes].forEach((chld) => {
                    tmpl.appendChild(chld);
                });

                tmpl.normalize();

                delete (tmpl.dataset.content);
                renderChildren = false;
            }

            // attributes
            [...tmpl.attributes].forEach((x) => {
                if (x.name.search(/\{.+\}/) > -1) {
                    let y = this.findAndReplaceBindings(x.name).textContent.match(/^(.*?)(\s*=\s*(["\'])?(.+)\3)?$/);

                    tmpl.removeAttribute(x.name);
                    if (y !== null && y[1].length) {
                        tmpl.setAttribute(y[1], typeof y[4] !== 'undefined' && y[4].length ? y[4] : y[1]);
                    }
                }
                else {
                    x.value = this.findAndReplaceBindings(x.valueOf().textContent).textContent;
                }
            });

            // standard nested children
            if (renderChildren) {
                [...tmpl.childNodes].forEach((chld) => {
                    chld.replaceWith(this.render(chld));
                    chld.normalize();
                });
            }

            // update form element selection attribute
            if (typeof tmpl.attributes.name !== 'undefined') {
                this.setFormInputSelectionAttribute(tmpl);
            }
        }
        else if (tmpl instanceof Node) {
            tmpl.replaceWith(...this.findAndReplaceBindings(tmpl.textContent).childNodes);
            tmpl.normalize();
        }

        tmpl = tmpl instanceof DocumentFragment && tmpl.children.length ? tmpl.children[0] : tmpl;

        if (updateDOM) {
            this.dom = tmpl;
        }
        return tmpl;
    }

    /**
     * Updates the DOM with updated value or structure triggered with `set()` or `del()` methods.
     *
     * @private
     * @param {string}  path - path that is being seeked-out and replaced with updated value
     * @param {Element} node - current rendered node element
     * @param {Element} rend - rendered template element
     */
    update(path, node, tmpl) {
        node = node || this.dom || this.render();
        tmpl = tmpl || this.template.cloneNode(true);

        this.dbgr(path, '>');
        this.dbgr(node);
        this.dbgr(tmpl);

        // No rendered DOM available, no need to make any update.
        if (node == null) {
            this.dbgr(null, '<');
            return;
        }

        const re = typeof path !== 'undefined'
            ? new RegExp('\{' + path + '(\..+)?\}', 'g')
            : new RegExp('\{.+?\}', 'g');

        if (tmpl instanceof DocumentFragment) {
            [...tmpl.childNodes].forEach((x) => {
                this.update(path, node, x);
            });
        }
        else if (tmpl instanceof Element) {
            let updateChildren = true;

            // conditional rendering
            if (typeof tmpl.dataset.if !== 'undefined') {
                let condition = this.elem.get(tmpl.dataset.if);

                if (
                    null === condition
                    || (typeof condition === 'boolean' && !condition)
                    || (typeof condition === 'number' && !condition)
                    || (condition instanceof Number && !condition)
                    || (condition instanceof String && !condition.length)
                    || (condition instanceof Array && !condition.length)
                ) {
                    // [...node.parentElement.childNodes].forEach((chld) => chld.remove());
                    node.remove();
                    this.dbgr('deleting children');
                    this.dbgr('dynamic.select');
                    this.dbgr(condition);
                    // this.dbgr(node.parentElement);
                    this.dbgr(null, '<');
                    return;
                }

                delete (tmpl.dataset.if);
            }

            // dataset content
            if (typeof tmpl.dataset.content !== 'undefined') {
                updateChildren = false;

                [...node.childNodes].forEach((childNode) => {
                    childNode.remove();
                });

                if (node instanceof Element) {
                    const rend = this.render(tmpl.cloneNode(true));
                    [...rend.childNodes].forEach((childNode) => {
                        node.appendChild(childNode);
                    });
                }
                delete (tmpl.dataset.content);
            }

            // attributes
            [...tmpl.attributes].forEach((x, idx) => {
                if (-1 < x.name.search(re)) {
                    let y = this.findAndReplaceBindings(x.name).textContent.match(/^(.*?)(\s*=\s*(["\'])?(.+)\3)?$/);

                    if (y !== null && y[1].length) {
                        node.setAttribute(y[1], typeof y[4] !== 'undefined' && y[4].length ? y[4] : y[1]);
                    }
                    else if (typeof node.attributes[idx] !== 'undefined') {
                        node.removeAttribute(node.attributes[idx].name);
                    }
                }
                else if (-1 < x.value.search(re)) {
                    node.setAttribute(x.name, this.findAndReplaceBindings(x.valueOf().textContent).textContent);
                }
            });

            // children
            if (updateChildren) {
                [...tmpl.childNodes].forEach((childNode, idx) => {
                    if (typeof node.childNodes[idx] === 'undefined') {
                        if (typeof node.append !== 'function') {
                            node.replaceWith(this.render(tmpl));
                            return;
                        }
                        else {
                            node.append(new Text());
                        }
                    }

                    this.update(path, node.childNodes[idx], childNode);

                    // if (typeof node.childNodes[idx] === 'undefined') {
                    //     node.replaceWith(this.render(tmpl));
                    // }
                    // else {
                    //     this.update(path, node.childNodes[idx], childNode);
                    // }
                });
                node.normalize();
            }

            if (typeof node.attributes !== 'undefined' && typeof node.attributes.name !== 'undefined') {
                this.setFormInputSelectionAttribute(node);
            }
        }
        else if (tmpl instanceof Node && (-1 < tmpl.textContent.search(re))) {
            const nodeParent = node.parentElement;
            const replacement = this.findAndReplaceBindings(tmpl.textContent);

            if (!replacement.childNodes.length) {
                [...nodeParent.childNodes].forEach((chld) => chld.remove());
            }
            else {
                if (nodeParent != null) {
                    [...nodeParent.childNodes].forEach((chld) => {
                        chld.remove();
                    });

                    nodeParent.append(...replacement.childNodes);
                }
            }
        }
        this.dbgr(null, '<');
    }

    /**
     * Checkes whether the Element node is select|checkbox|radio button
     * and sets appropriate attribute if its value is set in the data..
     *
     * @param {Element} node - current node element
     */
    setFormInputSelectionAttribute(node) {
        if (!this.elem.has(node.attributes.name.value))
            return;

        let values = this.elem.get(node.attributes.name.value);

        if (node.tagName == 'SELECT') {
            if (!(values instanceof Array)) {
                values = values != null ? [values] : [];
            }
            [...node.options].forEach((x) => {
                x.removeAttribute('selected');

                for (let i = 0; i < values.length; i++) {
                    if (values[i] == x.value) {
                        x.selected = true;
                        x.setAttribute('selected', 'selected');
                        break;
                    }
                }
            });
        }
        else if (
            node.tagName == 'INPUT'
            && typeof node.attributes.type !== 'undefined'
            && ['checkbox', 'radio'].includes(node.attributes.type.value)
        ) {
            node.checked = false;
            node.removeAttribute('checked');
            if (values instanceof Array) {
                for (let i = 0; i < values.length; i++) {
                    if (values[i] == node.value) {
                        node.checked = true;
                        node.setAttribute('checked', 'checked');
                        break;
                    }
                }
                return;
            }
            
            if (node.attributes.type == 'radio' && values == node.value) {
                node.checked = true;
                node.setAttribute('checked', 'checked');
                return;
            }

            if (node.value === 'on' ? values : values == node.value) {
                node.checked = true;
                node.setAttribute('checked', 'checked');
                return;
            }
        }
        else if (node.tagName == 'TEXTAREA') {
            node.value = node.textContent;
        }
    }

    /**
     * Generates default event listener on the form inputs.
     *
     * When an element's `tagName` is found in the switch-case, an event
     * listener with corresponding event is generated to store element's value
     * into Elem's props when the value changes. The event is generated only
     * for elements that carry a `name` attribute; those without it are ignored.
     *
     * @private
     * @param {Element} node - an element to be checked and "eventualized"
     */
    invokeAutoEvents(node) {
        let event = null;

        if (typeof node.attributes.name === 'undefined')
            return;

        switch (node.tagName) {
            case 'INPUT':
                switch (node.attributes.type.value) {
                    case 'text':
                    case 'number':
                    case 'phone':
                    case 'email':
                    case 'tel':
                    case 'url':
                    case 'range':
                    case 'color':
                        event = 'input';
                        break;

                    case 'radio':
                    case 'checkbox':
                    default:
                        event = 'change';
                        break;
                }
                break;
            case 'SELECT':
                event = 'change';
                break;

            case 'TEXTAREA':
                event = 'input';
                break;

            case 'BUTTON':
                event = 'click';
                break;

            default:
                return;
        }

        if (event == null)
            return;

        let pasivity = 0 && event.search('^touch.*') == 0 ? {passive: true} : false;

        node.addEventListener(event, (ev) => {
            ev.preventDefault();
            ((ev, me) => {
                let value;

                if (
                    ev.target.tagName == 'INPUT'
                    && ev.target.type == 'checkbox'
                ) {
                    value = this.elem.has(ev.target.name) ? this.elem.get(ev.target.name) : [];

                    if (value instanceof Array) {
                        if (ev.target.checked) {
                            value.push(ev.target.value);
                        }
                        else {
                            value.splice(value.indexOf(ev.target.value), 1);
                        }
                    }
                    else {
                        value = ev.target.checked ? true : false;
                    }
                }
                else if (
                    ev.target.tagName == 'SELECT'
                    && ev.target.hasAttribute('multiple')
                ) {
                    value = [];
                    [...ev.target.selectedOptions].forEach((x) => {
                        value.push(x.value);
                    });
                }
                else {
                    value = ev.target.value;
                }

                this.elem.set(ev.target.name, value);
            })(ev, this.elem);
        }, pasivity);
    }

    /**
     * Sets event listeners to given element.
     *
     * If an element carries a `data-e` attribute ("e" stands for "event"),
     * the content of the `dataset.e` is parsed. There can be multiple callbacks
     * defined, separated by space. Each callback is defined by listener event
     * and function name in following format:
     *
     * event:function
     *
     * The listener event is default event of `addEventListener()` function and
     * function name is a path to the Elem's props attribute.
     *
     * Generated function has 2 arguments - 1st argument is default target event
     * and 2nd argument is Elem itself.
     *
     * @private
     * @param {Element} element - DOM element to be enriched by events
     * @param {string} datasetE - dataset.e to be parsed
     */
    findAndInvokeEvents(element, datasetE) {
        let matches = datasetE.matchAll(/([a-z\d_|]+?):([a-z\d_.]+)/gi);
        let match;

        do {
            match = matches.next();

            if (typeof match.value === 'undefined')
                continue;

            let event = this.elem.hooks[match.value[2]];

            if (typeof event === 'function') {
                match.value[1].split('|').forEach((x) => {
                    let pasivity = 0 && x.search('^touch.*') == 0 ? {passive: true} : false;
                    element.addEventListener(
                        x,
                        (ev) => {
                            ev.preventDefault();
                            event(ev, this.elem)
                        },
                        pasivity
                    );
                });
            }
            else {
                console.error(
                    'Could not invoke `'
                    + match.value[1]
                    + '` listener, `'
                    + match.value[2]
                    + '` is not a function.'
                );
            }

        }
        while (!match.done);
    }

    /**
     * Returns DocumentFragment or Node with content generated by Elem's props.
     *
     * If an Elem's template carries a {variable} in any attribute or in
     * textContent itself, it is replaced by corresponding Elem's props value.
     * The variable is passed into Elem.get() method and based on its
     * instance it is transfered either into DocumentFragment or Node.
     *
     * @private
     * @param {Node} strNodeValue - string of a node containing {variable}
     */
    findAndReplaceBindings(strNodeValue) {
        let match;
        let matches = strNodeValue.matchAll(/\{(.+?)\}/g);
        let tempFragment = document.createDocumentFragment();

        do {
            match = matches.next();

            if (typeof match.value !== 'undefined') {
                let matchIndex = strNodeValue.indexOf(match.value[0]);
                let clearText = strNodeValue.substring(0, matchIndex);

                if (clearText.length) {
                    tempFragment.appendChild(new Text(clearText));
                }
                strNodeValue = strNodeValue.substring(matchIndex + match.value[0].length);

                let propsValue = this.elem.get(match.value[1]);
                let nv = this.nodizeValue(propsValue);

                tempFragment.append(...nv.childNodes);
            }
        }
        while (!match.done);

        if (strNodeValue.length) {
            tempFragment.append(new Text(strNodeValue));
        }

        return tempFragment;
    }

    /**
     * Translates given value into a Node.
     *
     * Takes any value and, based on its instance, translates it either into
     * Element or a Text Node.
     *
     * @param {mixed} propsValue - node string value translated into Node
     * @return {DocumentFragment}
     */
    nodizeValue(propsValue) {
        let fragment = document.createDocumentFragment();

        if (typeof propsValue === 'function') {
            return this.nodizeValue(propsValue());
        }
        else if (propsValue instanceof Array) {
            propsValue.forEach((x) => {
                let nv = this.nodizeValue(x);
                fragment.append(...nv.childNodes);
            });
        }
        else if (propsValue instanceof Element) {
            fragment.append(propsValue);
        }
        else if (propsValue instanceof Elem) {
            let propsNode;
            if (propsValue.renderer.dom == null) {
                propsNode = propsValue.renderer.render();
            }
            else {
                propsNode = propsValue.renderer.useDOM
                    ? propsValue.renderer.dom
                    : propsValue.renderer.dom.cloneNode(true);
            }
            fragment.append(propsNode);
        }
        else if (typeof propsValue !== 'object') {
            fragment.append(new Text(propsValue));
        }
        else if (propsValue == null) {
            fragment.append(new Text());
        }
        else {
            fragment.append(new Text(JSON.stringify(propsValue)));
        }


        return fragment;
    }
}

export {Flw, Elem};