import {Flw, Elem} from '/flw.js';

const flw = new Flw('.examples');

const app = new Elem('app', {
    author: flw.info().author,
    version: flw.info().version,
    url: 'https://bitbucket.org/Jocho/flwjs/src/master/'
});

flw.addElem(app);

flw.run();

const info = flw.info();

document.querySelector('.version').innerHTML = info.version;